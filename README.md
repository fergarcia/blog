Blog app
========

This is the **blog app** for Coursera's [Web Application Architectures](https://class.coursera.org/webapplications-001), by Greg Heileman.

The app it's being developed in Ruby on Rails 4.x, following the course specs.

NOTE: *This repository has been created for assignment evaluation purposes.*